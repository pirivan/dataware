#!/usr/bin/env python

import sys
import pandas as pd


def clean_up(filename):
    """ Clean up music data. Returns a pandas DataFrame """

    dfs = pd.read_html(filename)
    songs = pd.concat(dfs)
    songs.drop(['Unnamed: 2', 'Unnamed: 5', 'Unnamed: 6'], axis=1, inplace=True)
    songs.dropna(inplace=True)
    songs.drop_duplicates('#', inplace=True)
    songs.drop('#', inplace=True, axis=1)
    songs.sort_values(by=['Artist', 'Album', 'Name'], inplace=True)
    songs.reset_index(inplace=True)
    return songs.reindex(columns=['Artist', 'Album', 'Name'])


def to_csv(filename):
    """ Change file name prefix to '.csv' """

    from pathlib import Path
    path = Path(filename)
    return path.with_suffix('.csv')


if __name__ == '__main__':
    html = sys.argv[1]
    csv = to_csv(html)
    df = clean_up(html)
    df.to_csv(csv)
    print('saved file {} with {} songs\n'.format(csv, len(df)))

