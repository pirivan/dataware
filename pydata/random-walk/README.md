# Random Walk Visualization

This program creates a graphic file containing information about two 2D random
walks. The information for each random walk includes a plot of the walk and a
plot of the distance from the origin as a function of the number of steps.

The first random walk has equally-sized steps drawn from a uniform probability
distribution. The second one draws the step sizes from a Normal distribution
centred around zero.

The program accepts the following command-line options:

- The number of steps. The default is 1000.

- The standard deviation to use for the Normal distribution. The default is 0.5.

- The number of random walk samples of each kind to generate. If this number is
  greater than one, which is the default, the program averages all samples
  before producing the final graphic file. Mathematically speaking, averaging
  random walks does not make much sense. This is done as an exercise on using
  the Numpy library more extensively.

Use the program as follows:

```buildoutcfg
random-walk.py -h
usage: random-walk.py [-h] [--samples SAMPLES] [--steps STEPS] [--scale SCALE]

Visualize Random Walks

optional arguments:
  -h, --help         show this help message and exit
  --samples SAMPLES  number of samples
  --steps STEPS      number of steps
  --scale SCALE      std variance
```

The name of the output file is hard-coded to "rw.png" in the current directory.
The following figure is a sample output from the program:

![sample](rw-sample.png)