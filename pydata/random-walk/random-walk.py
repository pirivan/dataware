#!/usr/bin/env python

"""Random Walk Visualization

This program creates a graphic file containing information about two 2D random
walks. The information for each random walk includes a plot of the walk and a
plot of the distance from the origin as a function of the number of steps.

The first random walk has equally-sized steps drawn from a uniform probability
distribution. The second one draws the step sizes from a Normal distribution
centred around zero.

The program accepts the following command-line options:

- The number of steps. The default is 1000.

- The standard deviation to use for the Normal distribution. The default is 0.5.

- The number of random walk samples of each kind to generate. If this number is
  greater than one, which is the default, the program averages all samples
  before producing the final graphic file. Mathematically speaking, averaging
  random walks does not make much sense. This is done as an exercise on using
  the Numpy library more extensively.

The name of the output file is hard-coded to "rw.png" in the current directory.
"""

import numpy as np
import matplotlib.pyplot as plt


def random_walks(nwalks, nsteps, scale=0.):
    """  Generate one or more 2D random walks

    This function generates a number of 2D random walks represented as the
    matrix "walks" of size 2*nwalks x nsteps, as follows:

    - First random walk: walks[0] (X-dimension) and walks[1] (Y-dimension). The
      first step from the origin (0,0) is then to (walks[0][0], walks[1][0])
      coordinates.

    - Second random walk: walks[2] and walks[3]

    - Nth random walk: walks[2*(N-1)] and walks(2*(N-1)+1)

    Parameters
    ----------
    nwalks : int
        The number of random walks to generate

    nsteps : int
        The number of steps

    scale : float
        The standard deviation for the Normal distribution.
        The default is value is zero.

        This parameter serves an additional purpose. If it is zero, the
        default, the step sizes are drawn from a uniform distribution and
        adjusted to become equally-sized. Otherwise, the step sizes are drawn
        from a Normal distribution centred at zero and with the specified
        standard deviation.

    Returns
    -------
    walks: np.array
        The walks' coordinates matrix as described above.

    """
    
    if scale:
        steps = np.random.normal(loc=0, scale=scale, size=(nwalks*2, nsteps))
    else:
        draws = np.random.randint(0, 2, size=(nwalks*2, nsteps))
        steps = np.where(draws > 0, 1,-1)
    walks = steps.cumsum(axis=1)
    return walks


def walks_average(walks, ints=False):
    """Average walk steps and calculate the distance vector

    This function generates a single random walk by calculating the average
    position of multiple random walks at each step. Once this is done, this
    function calculates the distance vector for the said single random walk.

    Parameters
    ----------
    walks : np.array
        The coordinates matrix for the walks.

    ints : bool
        A flag used to convert the single random walk coordinate values to integers.
        This is useful when the random walks come originally as equally-spaced steps.
    
    Returns
    -------
    walk
        The 2-row matrix of the averaged step positions.

    distance
        The distance vector of the averaged random walk. Each element of this
        vector is calculated as the Euclidean distance from the origin to the
        random walk position at the corresponding step.

    """

    _, nsteps = walks.shape
    walk = np.empty((2, nsteps))
    walk[0] = walks[::2].mean(axis=0)
    walk[1] = walks[1::2].mean(axis=0)
    if ints:
        walk[0] = walk[0].astype(int)
        walk[1] = walk[1].astype(int)
    distance = np.sqrt(walk[0] ** 2 + walk[1] ** 2)
    return walk, distance   


def adjust_rw_plot(ax, right=False):
    """Adjust the axis of random walk plots

    This function adjust the top and one of the left or right axis to cross at
    the origin (0,0). It also adjusts the position of the vertical ticks to
    either left or right. 

    Parameters
    ----------
    ax : subplot
        The figure subplot to operate on.

    right : bool
        A flag used to indicate if the subplot is to be placed on the right of
        the figure.

    """
    axdata = 'right'
    axtics = 'left'
    if right:
        axdata = 'left'
        axtics = 'right'
    ax.spines['top'].set_position(('data',0))
    ax.xaxis.set_ticks_position('bottom')
    ax.spines[axdata].set_position(('data',0))
    ax.yaxis.set_ticks_position(axtics)
    ax.spines['left'].set_smart_bounds(True)
    ax.spines['right'].set_smart_bounds(True)


def adjust_d_plot(ax, right=False):
    """Adjust the axis of distance plots

    This function sets the labels of the distance plots.

    Parameters
    ----------
    ax : subplot
        The figure subplot to operate on.

    right : bool
        A flag used to indicate if the subplot is to be placed on the right of
        the figure.

    """
    axlabel = 'left'
    if right:
        axlabel = 'right'
    else:
        ax.set_ylabel("distance")
    ax.yaxis.set_label_position(axlabel)
    ax.yaxis.set_ticks_position(axlabel)
    ax.set_xlabel("steps")


def parse_cli():
    import argparse

    parser = argparse.ArgumentParser(description='Visualize Random Walks')
    parser.add_argument('--samples', type=int, default=1,
                        help='number of samples')
    parser.add_argument('--steps', type=int, default=1000,
                        help='number of steps')
    parser.add_argument('--scale', type=float, default=0.5,
                        help='std variance')
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_cli()
    samples = args.samples
    steps = args.steps
    scale = args.scale
    ws0 = random_walks(samples, steps)
    w0, d0 = walks_average(ws0, ints=True)
    ws1 = random_walks(samples, steps, scale=scale)
    w1, d1 = walks_average(ws1)

    fig = plt.figure()
    title = "2D Random Walks (1 sample)"
    if samples > 1:
        title = "2D Random Walks ({} samples)".format(samples)
    fig.suptitle(title)

    # top-left subplot
    w = fig.add_subplot(221)
    w.plot(w0[0], w0[1], color='blue')
    w.set_title("Equally-spaced steps".format(scale))
    adjust_rw_plot(w)

    # top-right subplot
    w = fig.add_subplot(222)
    w.plot(w1[0], w1[1], color='red')
    w.set_title("Normally-spaced steps ($\sigma = {}$)".format(scale))
    adjust_rw_plot(w, right=True)

    # bottom-left subplot
    d = fig.add_subplot(223)
    adjust_d_plot(d)
    d.plot(d0, color='blue')

    # bottom-right subplot
    d = fig.add_subplot(224)
    adjust_d_plot(d, right=True)
    d.plot(d1, color='red')

    # save figure
    fig.savefig('rw.png', dpi=133)
